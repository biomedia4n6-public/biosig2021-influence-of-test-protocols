## Contents

Contents of this README file:

- [Introduction](#introduction)
    - [System compatibility](#system-compatibility)
    - [Code functionality](#code-functionality)
- [Installation](#installation)
    - [Windows](#windows)
    - [macOS](#macos)
- [Experimental pipeline](#experimental-pipeline)
- [How results are generated](#how-results-are-generated)
- [Running experiments](#running-experiments)
    - [Quick reproduction](#quick-reproduction)
    - [Reproducing BOB UTFVP experiments](#reproducing-bob-utfvp-experiments)
- [Files and their purpose](#files-and-their-purpose)
    - [utils.py](#utilspy)
        - [user-changeable parameters](#user-changeable-parameters)
        - [class Score](#class-score)
    - [generate_score_object.py](#generate_score_objectpy)
    - [generate_results.py](#generate_resultspy)
- [Contact](#contact)

For a quick try to reproduce the results, please look at section [Installation](#installation) ( for [Windows](#windows)
and [macOS](#macos)) and section [Quick reproduction](#quick-reproduction).

## Introduction

This code generates the results summarised in the publication:

```bibtex
@inproceedings{Eglitis2021,
author = {Eglitis, Teodors AND Maiorana, Emanuele AND Campisi, Patrizio},
title = {Influence of Test Protocols on Biometric Recognition Performance Estimation},
booktitle = {BIOSIG 2021 - Proceedings of the 20th International Conference of the Biometrics Special Interest Group},
year = {2021},
editor = {Brömme, Arslan AND Busch, Christoph AND Dantcheva, Antitza AND Raja, Kiran AND Rathgeb, Christian AND Uhl, Andreas} ,
pages = { 1-8 },
publisher = {Gesellschaft für Informatik e.V.},
address = {Bonn}
}
```

We hope that this code will be helpful to others, e.g., to bring up to speed with testing in biometrics undergraduate
students.

### System compatibility

The code is based on the signal processing and machine learning toolbox [Bob](https://www.idiap.ch/software/bob/),
developed by Idiap Research Institute, Switzerland. The BOB is designed to run on Unix systems (Linux and macOS); we
have tested this code
using ``Windows 10 Enterprise version 20H2 with Windows Subsystem for Linux version 2 (WSL2) running Ubuntu-20.04`` as
well as ``macOS Big Sur version 11.5.2 (20G95)``. We haven't tested code using a Linux stand-alone system, but there is
no reason why it shouldn't work under Linux.

### Code functionality

Code found in this repository generates figures and data published in our paper:

![ROC curves](./doc/fig_2.png "ROC curves")
![EER results](./doc/tab_2.png "EER results")

## Installation

In this section, you can find quick instructions on how to install BOB on your system. For more detailed instructions
please look at [BOB installation instructions](https://www.idiap.ch/software/bob/docs/bob/docs/stable/install.html) and
BOB's documentation in [general](https://www.idiap.ch/software/bob/).

### Windows

1) Install ``Windows Subsystem for Linux version 2 (WSL2). ``
   . [Here](https://docs.microsoft.com/en-us/windows/wsl/install-win10) you can find detailed instructions on how to do
   it. **The following commands you will have to execute on the WSL (Ubuntu), instead of Windows**;

2) Install Miniconda on the Windows Subsystem (e.g. Ubuntu). You can do that by downloading a Miniconda installer
   -- [link](https://docs.conda.io/en/latest/miniconda.html#linux-installers). The newest one will do just fine. After
   downloading the installer file (e.g., ``Miniconda3-py39_4.10.3-Linux-x86_64.sh``), you have to open
   the ``Windows Terminal``, choosing a new WSL tab (in our case, ``Ubuntu-20.04``). First, you have to navigate to the
   directory where you downloaded the file, e.g., if you used the Windows Download
   directory ``C:\Users\<username>\Downloads``, on the Linux Subsystem the command is:

    ```shell
    <username>@DESKTOP-5V79QQO:/mnt/c/Users/<username>/Downloads$ cd /mnt/c/Users/<username>/Downloads
    ```
   Where the ``<username>`` is your username (without the chevrons). After, run the Miniconda installer:

    ```shell
    $ bash Miniconda3-py39_4.10.3-Linux-x86_64.sh
    ```

   and follow the installation instructions. Also, it would be handy to update the newly installed Conda by running
   command:

    ```shell
    $ conda update conda
    ```

3) Now, we can create a Conda Python environment containing the necessary BOB packages. The command for creating the
   environment named ``BOB``:

    ```shell
    $ conda create --name BOB --override-channels \
      -c https://www.idiap.ch/software/bob/conda -c defaults \
      python=3 bob.bio.vein bob.db.utfvp
    ```

4) (optional) If you want to display fancy characters in the plots generated (as can be seen in the original
   publication), you need to install Latex in the WLS (even if you already have Latex installed on the Windows itself).
   This can be done by running the command:

    ```shell
    $ sudo apt update
    $ sudo apt-get install texlive-latex-extra
    ```

   The ``texlive-latex-extra`` will take approx. 464 MB of the disk space after installation.

### macOS

1) Install Miniconda. You can do that by downloading a Miniconda macOS installer
   -- [link](https://docs.conda.io/en/latest/miniconda.html#macosx-installers). The newest one will do just fine. After
   downloading the installer file (e.g., ``Miniconda3-py39_4.10.3-MacOSX-x86_64.sh``), open the terminal. First, you
   have to navigate to the directory where you downloaded the file, e.g., if you used the default Download
   directory ``C:\Users\<username>\Downloads``, on the Linux Subsystem the command will be:

    ```shell
    % cd /Users/<username>/Downloads
    ```

   After, run the Miniconda installer:

    ```shell
    % bash Miniconda3-py39_4.10.3-MacOSX-x86_64.sh
    ```

   And follow the installation instructions. Also, it would be handy to update the newly installed conda by running
   command:

    ```shell
    % conda update conda
    ```

2) Now, we can create a Conda Python environment containing the necessary BOB packages. The command for creating the
   environment named ``BOB``:

    ```shell
    % conda create --name BOB --override-channels \
      -c https://www.idiap.ch/software/bob/conda -c defaults \
      python=3 bob.bio.vein bob.db.utfvp
    ```

3) (optionally) If you want to display fancy characters in the plots generated (as can be seen in the original
   publication), you need to install Latex. *A detailed installation instructions will be added later -- we haven't
   found time to test this software on a fresh macOS installation and don't know if the Latex version shipped with macOS
   contains all the necessary libraries*.

## Experimental pipeline

This section is briefly outlined the main idea of the experiment presented in the research paper. We are using the UTFVP
database (can access the original biometric data by filling
a [download request form](https://www.utwente.nl/en/eemcs/dmb/downloads/utfvp/)). One of the most popular experimental
pipelines for this database uses Maximum Curvature feature extractor and Miura Match matching algorithm:

![pipeline](./doc/pipeline.png "pipeline")

The Miura Match algorithm is not symmetrical -- comparing ``Finger 1`` image ``1`` with ``Finger 1`` image ``2``, the
result **can** be different when comparing the images in the opposite order. The maximum possible similarity in the
experiment is ``0.5`` (a higher value indicates closer similarity). In the following image, we can see all possible
score types (on a two-class example):

![Different score types](./doc/fig_1.png "Different score types")

This pipeline is implemented in
BOB's [bob.bio.vein](https://www.idiap.ch/software/bob/docs/bob/docs/stable/bob/bob.bio.vein/doc/index.html).

## How results are generated

Idiap's ``BOB`` software consists of experimental frameworks for various modalities, including vascular modalities,
called [bob.bio.vein](https://www.idiap.ch/software/bob/docs/bob/docs/stable/bob/bob.bio.vein/doc/index.html). in it,
popular vascular biometrics baselines are implemented, including the one considered here.

Using the ``BOB`` protocol ``full``, we generate all possible $`1440^2=2'073'600`$ scores. Since we know which score
corresponds to which two template comparisons (and we know the order of comparison -- which one enrolled template and
which -- probe), then we can use these scores to calculate results for the exact experimental pipeline for different
protocols and thus evaluate how different protocols affect the results on this particular database and experimental
setup.

``BOB`` software outputs score file ``scores-dev`` for the ``development`` database subset, in protocol ``full``, all
$`1440`$ files are added to this subset. The ``scores-dev`` consists of a list of scores in the
so-called ``Bob 4-column feature vector``. From their documentation:

```text
Info about Bob 4-column feature vector:
claimed_id real_id test_label score
   str – The claimed identity – the client name of the model that was used in the comparison
   str: The real identity – the client name of the probe that was used in the comparison
   str: A label of the probe – usually the probe file name or the probe id
   float: The result of the comparison of the model and the probe
```

For example:

```text
0001_1 0002_3 0002/0002_3_4_120523-103359 0.04273350
```

However, as we can see, the ``claimed identity`` (enrol template) -- ``0001_1`` and the ``real identity`` (probe
template) ``0002_3``. The ID allows us to identify the subject and the finger in each case, but there is no information,
which image (1 to 4) was used each time. The third row, ``label of the probe, `` uses the full image name to obtain the
information about the probe file image number (``4`` in the example given). Luckily, before the final ``scores-dev``
file is generated, the calculation output for each enrol template is written in a separate file to enable Bob to execute
multiple experiments simultaneously. These intermediate result files are saved in folder ``dev``. Each file is named by
the enrol template's, e.g., ``0002_3_4`` -- also containing the image number.

## Running experiments

In this section, we describe how to run experiments. Before trying to run the experiments, please refer to the
section [Installation](#installation) ( for [Windows](#windows) and [macOS](#macos)).

### Quick reproduction

To reproduce results (and figures) generated by this work will take only a few minutes. All you have to do is (using a
terminal):

* copy/clone this repository:
    ```shell
    $ git clone https://gitlab.com/biomedia4n6-public/biosig2021-influence-of-test-protocols.git
    ```
  Change directory to the one you just downloaded:
    ```shell
    $ cd ./biosig2021-influence-of-test-protocols/
    ```
* activate the Conda environment created in section [Installation-windows](#windows) or [Installation-macOS](#macos):
    ```shell
    $ conda activate BOB
    ```
* run the Python script ``generate_results.py``:
    ```shell
    $ python ./generate_results.py
    ```

Note that you have to run these commands in the ``WSL`` console if you are using Windows. Alternatively, in PyCharm
Professional Edition (free for students and academic staff) you can add the ``WSL`` Conda virtual environments as the
Python interpreters, thus using ``WSL`` environments seamlessly without the need for console use.

To reproduce the entire experiment, please see
section [Reproducing BOB UTFVP experiments](#reproducing-bob-utfvp-experiments).

### Reproducing BOB UTFVP experiments

You can also reproduce the entire experiment. To do so, you need to follow the steps:

1) **Download the UTFVP database.** To access the original biometric data, you have to fill
   the [download request form](https://www.utwente.nl/en/eemcs/dmb/downloads/utfvp/)), download and extract files in a
   location you prefer. The next step is to tell Bob where is the database saved. To do that, you have to create a file
   in your username's home directory. E.g., for ``WSL`` it is ``\\wsl$\Ubuntu-20.04\home\<username>``. To navigate to
   it, using ``WSL``/``macOS`` terminal:
    ```shell
    $ cd ~
    ```
   create file entitled ``.bob_bio_databases.txt``:
    ```shell
    touch .bob_bio_databases.txt
    ```
   This file *joins* all the databases with their paths. For the UTFVP database, add the line in the
   file, ``[YOUR_UTFVP_DIRECTORY] = <location>``, where the ``<location>`` is where you saved the data. For example, in
   our case:
    ```text
    [YOUR_UTFVP_DIRECTORY] = /home/<username>/DATABASES/utfvp/data    
    ```
   BOB uses a so-called database manager, which needs information on the various protocol relationship with the database
   in question. These files aren't delivered with the ``BOB`` software but need to be downloaded separately. This can be
   done by running a command:
   ```shell
    $ conda activate BOB
    $ bob_dbmanage.py all download --missing
    ```
2) **Re-run the protocol ``full`` pipeline in question.**
   Now we should be able to run an entire BOB experiment. The computation time should be a few hours, depending on your
   system. To rerun the UTFVP database protocol ``full`` using the ``mc`` baseline, you have to run command:
   ```shell
   $ conda activate BOB
   $ verify.py utfvp mc --protocol full --groups dev \
   --parallel 6 \
   -T /home/<username>/TEMP -R /home/<username>/RESULTS -vv
   ```
   Here: ``--parallel 6`` indicates the CPU cores on your system; ``-T /home/<username>/TEMP`` directory where temporary
   files will be saved, and ``-R /home/<username>/RESULTS`` -- where the result files will be saved (we need these!).

   For more information, you can call help on the ``BOB's`` verify function: ``verify.py -h`` or see
   the ``bob.bio.vein``
   documentation [here](https://www.idiap.ch/software/bob/docs/bob/docs/v7.0.0/bob/bob.bio.vein/doc/baselines.html).
   Note -- currently, in ``BOB`` there is a new mechanism for executing baseline biometrics experiments; we are using
   the *old* methods, which is why the documentation link points to the BOB version ``v7.0.0``.

   After experiment execution is finished, you can calculate the EER by using BOB tools:
    ```shell
    $ bob bio metrics -c eer /home/<username>/RESULTS/mc/full/nonorm/scores-dev 
    ```
   The output should be:
   ```text
    =====================  ====================
    ..                     Development
    =====================  ====================
    Failure to Acquire     0.0%
    False Match Rate       0.6% (12874/2067840)
    False Non Match Rate   0.6% (36/5760)
    False Accept Rate      0.6%
    False Reject Rate      0.6%
    Half Total Error Rate  0.6%
    =====================  ====================
    ```


3) **Generate the score list file.** Now, we can start using scripts found in this repository. For score list file
   generation, please see section [generate_score_object.py](#generate_score_objectpy)
4) **Generate the results.** Please go to the section [generate_results.py](#generate_resultspy).

## Files and their purpose

### ``utils.py``

This file contains:

* package imports;
* user-changeable parameters -- e.g. paths for used files;
* class ``Score`` definition;
* other help files.

#### user-changeable parameters

Parameters that the user can change are as follows:

```python
OUTPUT_PLOT_PATH = "./graphics/"
SCORE_OBJECT_FILE_PATH = "./help_files/SCORE_LIST.dat"
DATABASE_SCORE_DIRECTORY = "../RESULTS/mc/full/nonorm/dev/"
FORCE_NOT_USE_LATEX = False
LATEX_SETTINGS_FILE = './help_files/LATEX_SETTINGS.txt'
```

In more detail:

* ``OUTPUT_PLOT_PATH`` -- directory where the plots will be saved;
* ``SCORE_OBJECT_FILE_PATH`` -- path, where the score object is saved/will be saved by the
  script ``generate_score_object.py``. We do provide the score object file with this repository, so this shouldn't be
  changed;
* ``DATABASE_SCORE_DIRECTORY`` -- (optional) -- path, where the calculated results for protocol ``full``
  , ``development`` subset is located. E.g., ``/full/nonorm/dev/``. To reproduce the results, the actual score files are
  not needed; the necessary information is saved in the Pickle file located in the ``SCORE_OBJECT_FILE_PATH`` address.
  The original results, recalculated by BOB, are needed if you want to reproduce the original BOB baseline experiment;
  more information can find in the section entitled [generate_score_object.py](#generate_score_objectpy).
* ``FORCE_NOT_USE_LATEX`` -- bool flag ``True``/``False``; default value is ``False``. If set to ``True``, latex font
  output is disabled (even if Latex installation is found in your system);
* ``LATEX_SETTINGS_FILE`` -- file path, where the ``LATEX_SETTINGS.txt`` text file is located. In the file are defined
  the specific Latex imports and symbols used by the ``Matplotlib`` library. The settings file is shipped with this
  repository, thus not likely that the paths would need to be changed.

Apart from these parameters, nothing else in this file should be changed.

#### class ``Score``

The class is used as the backbone of these scripts. The file ``generate_score_object.py`` from the original scores
generates a list of ``Score`` objects, creating one Score object for each score (there are $`1440^2=2'073'600`$ scores
in total). In the scripts, this list is called ``SCORE_LIST``. The score list is saved as a Pickle file and delivered
together with this repository. Later, when running the ``generate_results.py`` file, the same score list is loaded from
memory.
``score`` object properties are used to determine which protocols the score belongs to.

E.g., for ``SCORE_LIST[100]``, the initial parameters are:

```pythonregexp
enrol_s          = 1                    probe_s          = 5
enrol_f          = 1                    probe_f          = 2
enrol_i          = 1                    probe_i          = 1
                                        score            = 0.05782582
```

Meaning that enrol template was created using the UTFVP database image ``0001_1_1`` (1st subject, 1st finger, 1st image)
, and the probe -- using image ``0005_2_1`` (5th subject, 2nd finger, 1st image); the acquired similarity score
-- ``0.05782582``. The remaining parameters are generated within the ``Score`` class and for the score in question are:

```pythonregexp
genuine          = False                gen_identical    = False
imposter         = True                 gen_top          = False
test_class       = True                 gen_bott         = False
dev_class        = False                imp_top          = True
eval_class       = False                imp_bott         = False
parameter_tuning = False                finger_1         = True
par_tun_any      = True                 same_finger      = False
```

These bool parameters can be used to ease the searching for the appropriate scores for each protocol. For more
information, look at the code, or after ``utils.py`` file us run, call the documentation, e.g.:

```python
print(Score.__doc__)
```

For an easy overview of the scores, you can call the ``print`` method on a score object, e.g.:

```python
print(SCORE_LIST[100])
```

### ``generate_score_object.py``

**This file use is optional.**

The script uses the original scores calculated by the BOB framework and generates a list of ``Score`` objects, creating
one Score object for each score (there are $`1440^2=2'073'600`$ scores in total). In our code, this list is
called ``SCORE_LIST``. **The score list is saved as a Pickle file and delivered with this repository.** Meaning that
there is no real need to re-generate the score list, but this can be reused for other projects or if one wants to
reproduce the complete experiment.

In the ``utils.py`` file, the relevant input paths for the ``generate_score_object.py`` are:

```python
SCORE_OBJECT_FILE_PATH = "./help_files/SCORE_LIST.dat"
DATABASE_SCORE_DIRECTORY = "../RESULTS/mc/full/nonorm/dev/"
```

Please look at the subsection [user-changeable parameters](#user-changeable-parameters) for more information.

### ``generate_results.py``

This is a script containing the main experiment's functionality.

The main steps of the code are:

* Import the ``utils.py`` file and its contents, including the predefined user-changeable parameters:

    ```python
    from utils import *
    ```

* load the ``SCORE_LIST`` from the Python Pickle file shipped with this repository/generated by the user with
  the ``generate_score_object.py`` file;
* create figures where the ROC plots will be saved. Currently, five figures are created (one for each protocol type and
  one -- as summary figure);
* create ``experiment_definitions`` dictionary object. It has the following keys:
    * ``'gen_entries'`` -- a list of Genuine Score class objects that meets the given protocol requirements;
    * ``'imp_entries'`` -- a list of Impostor Score class objects that meets the given protocol requirements;
    * ``'figures'`` -- ``matplotlib.figure.Figure`` object (previously created figure) or list of them, where the
      obtained ROC curves will be plotted to;
    * ``'latex_name'`` -- protocols name, permitting Latex math syntax and custom-defined symbols;
    * ``'simple_name'`` -- protocol name, not using Latex syntax or symbols;
    * ``'label'`` -- protocol's name used when plotting the figure. As the ``label`` is assigned ``'latex_name'``
      or ``'simple_name'``, depending on if there is a Latex installation in the system (for Windows, in the ``WLS``).

  Score objects are assigned to the appropriate lists, exploiting the Score class variables, e.g.:

    ```python
    experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                                  x.imposter is True and
                                                  x.finger_1 is True and
                                                  x.imp_bott is True and
                                                  x.same_finger is True])
    ```
  The ``experiment_definitions`` items are lists, and the functionality is organised so
  that ``experiment_definitions[<key>][n]``, where $`n \in [0, 21]`$ (there are 22 protocols) correspond to a single
  protocol. E.g., ``experiment_definitions['gen_entries'][2]``, ``experiment_definitions['imp_entries'][2]``
  and ``experiment_definitions['label'][2]`` corresponds to protocol ``parameter tuning``.
* saving of figures;
* comparing the newly generated ``EER`` values with the ones calculated by us.

## Contact

For any questions, please open a
new [issiue](https://gitlab.com/biomedia4n6-public/biosig2021-influence-of-test-protocols/-/issues/new) or write me
at ``teodors.eglitis [at] uniroma3 [dot] it``