#!/usr/bin/env python3
"""
utils.py -- contains:
              general experiment settings e.g., input and output file paths;
              help functions;
              class ``Score`` definition.

Author -- Teodors Eglitis, teodors.eglitis@uniroma3.it
"""

import numpy as np
import bob.measure
import matplotlib.figure as mpl_fig
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
from distutils.spawn import find_executable
import _pickle as cPickle

"""
general settings, update if needed:
"""
OUTPUT_PLOT_PATH = "./graphics/"
SCORE_OBJECT_FILE_PATH = "./help_files/SCORE_LIST.dat"
DATABASE_SCORE_DIRECTORY = "../RESULTS/mc/full/nonorm/dev/"
FORCE_NOT_USE_LATEX = False
LATEX_SETTINGS_FILE = './help_files/LATEX_SETTINGS.txt'

if find_executable('latex') and FORCE_NOT_USE_LATEX is False:
    print("Found Latex installed, will be using fancy Latex symbols!")
    USE_LATEX = True
    mpl.use("pgf")
    # reading the ``LATEX_SETTINGS_FILE``:
    with open(LATEX_SETTINGS_FILE, 'r') as inf:
        params = eval(inf.read())
    # Setting custom Matplotlib LateX parameters to enable us to use self-defined symbols:
    mpl.rcParams.update(params)
else:
    print("Couldn't find a Latex installation, will be using simple characters")
    USE_LATEX = False

# creating output directory:
if not os.path.exists(OUTPUT_PLOT_PATH):
    os.makedirs(OUTPUT_PLOT_PATH)


def determine_parameter_tuning(subject, finger):
    """
    Function ``determine_parameter_tuning(subject, finger)`` returns True, if the finger in question belongs to the 35
    class parameter training subset.
        :param subject: (int) UTFVP finger vein database subject number;
        :param finger: (int) subject's finger used (numbered from 1 to 6);
        :return: True, if the given finger belongs to the 35 class parameter training subset; else returns
                 False
    """
    re = np.remainder(subject, 6)
    if re == 0:
        re = 6
    if subject in range(1, 36) and finger == re:
        flag = True
    else:
        flag = False
    return flag


class Score:
    """
    ``Score`` object class definition. For each score in the UTFVP finger-vein database, a score object is created, in
    order to correctly assign scores to the various protocols.
    Variables:

        self.enrol_s           (int): enrol subject number
        self.enrol_f           (int): enrol subject finger (1 to 6)
        self.enrol_i           (int): enrol subject finger image number (1 to 4)
        self.probe_s           (int): probe subject number
        self.probe_f           (int): probe subject finger (1 to 6)
        self.probe_i           (int): probe subject finger image number (1 to 4)
        # -----
        self.score             (float): comparison score between enrol and probe image. Using the current pipeline,
                                        scores are in range [0, 0.5], where 0.5 indicates a perfect similarity
        self.genuine           (bool): flag is set to True, if score is produced by enrol and probe templates from the
                                       same class (finger)
        self.imposter          (bool): flag is set to True, if score is produced by enrol and probe templates from
                                       different classes (fingers)
        self.test_class        (bool): For Idiap's NOM protocol -- flag is set to True, if both enrol and probe
                                       templates used to produce the score belongs to the ``test`` subgroup.
        self.dev_class         (bool): For Idiap's NOM protocol -- flag is set to True, if both enrol and probe
                                       templates used to produce the score belongs to the ``dev`` subgroup.
        self.eval_class        (bool): For Idiap's NOM protocol -- flag is set to True, if both enrol and probe
                                       templates used to produce the score belongs to the ``eval`` subgroup.
        self.parameter_tuning  (bool): Flag is set to True, if ``both`` --  enrol and probe templates belong to the
                                       Parameter Tuning 35-class subgroup (used in the original paper describing
                                       database). Can be used to select ``all`` scores within the Parameter Tuning
                                       protocol.
        self.par_tun_any       (bool): Flag is set to True, if ``any`` template -- enrol or probe belong to the
                                       Parameter Tuning 35-class subgroup. Can be used to filter out any scores produced
                                       using templates belonging to this subgroup.
        self.gen_identical     (bool): Flag is set to True, if score is calculated scoring template to itself. Also
                                       called ``genuine Same VS Same``. Visually, on the confusion matrix these scores
                                       appears on the ``diagonal`` see README for more info.
        self.gen_top           (bool): Flag is set to True, if score is genuine and on the confusion matrix is ``top``
                                       from the ``diagonal`` -- for the genuine class, ``enrol_i < probe_i``. see README
                                       for more info.
        self.gen_bott          (bool): Flag is set to True, if score is genuine and on the confusion matrix is located
                                       ``bottom`` from the ``diagonal`` -- for the genuine class, ``enrol_i > probe_i``.
                                       See README for more info.
        self.imp_top           (bool): Flag is set to True, if score is ``impostor`` and on the confusion matrix is
                                       located ``top`` from the ``diagonal``. See README for more info.
        self.imp_bott          (bool): Flag is set to True, if score is ``impostor`` and on the confusion matrix is
                                       located ``bottom`` from the ``diagonal``. See README for more info.
        self.finger_1          (bool): Flag is set to True, if both enrol and probe temples' uses image No 1 (the first
                                       of 4 images of a class in the database). Flag is useful for finding FVC
                                       protocols' impostor scores.
        self.same_finger       (bool): Flag is set to True, if both enrol and probe temples' belong to the same finger
                                       (e.g. right hand, middle finger). Flag is useful for finding FVC SHORT protocols'
                                       impostor scores.
    """

    def __init__(self,
                 enrol_s, enrol_f, enrol_i,
                 probe_s, probe_f, probe_i,
                 score):
        self.enrol_s = enrol_s
        self.enrol_f = enrol_f
        self.enrol_i = enrol_i
        self.probe_s = probe_s
        self.probe_f = probe_f
        self.probe_i = probe_i
        self.score = score
        # --------------- other parameters, for now, setting a default values, later they will be changed:
        self.genuine = False
        self.imposter = False
        # --------------- for Nom protocols:
        self.test_class = False
        self.dev_class = False
        self.eval_class = False
        # --------------- 35 class parameter tuning subgroup:
        self.parameter_tuning = False
        self.par_tun_any = False
        # --------------------------------------------------
        # Score groups to work with symmetrical/unsymmetrical scores:
        # Genuine scores:
        self.gen_identical = False
        self.gen_top = False
        self.gen_bott = False
        # Imposter scores:
        self.imp_top = False
        self.imp_bott = False
        # for the FVC protocol:
        self.finger_1 = False
        # For FVC protocol imposter comparisons, which use only 1st images from the same finger:
        self.same_finger = False
        # ------------------------------------------------------------------------
        if self.enrol_s == self.probe_s and self.enrol_f == self.probe_f:
            self.genuine = True
        else:
            self.imposter = True

        if self.enrol_s in range(1, 11) and self.probe_s in range(1, 11):
            self.test_class = True
        elif self.enrol_s in range(11, 29) and self.probe_s in range(11, 29) \
                and self.enrol_i in range(1, 3) and self.probe_i in range(3, 5):
            self.dev_class = True
        elif self.enrol_s in range(29, 61) and self.probe_s in range(29, 61) \
                and self.enrol_i in range(1, 3) and self.probe_i in range(3, 5):
            self.eval_class = True
        if determine_parameter_tuning(subject=self.enrol_s, finger=self.enrol_f) or \
                determine_parameter_tuning(subject=self.probe_s, finger=self.probe_f):
            self.par_tun_any = True
        # Scores used in Parameter Tuning protocol:
        if determine_parameter_tuning(subject=self.enrol_s, finger=self.enrol_f) and \
                determine_parameter_tuning(subject=self.probe_s, finger=self.probe_f):
            self.parameter_tuning = True
        # symmetrical/unsymmetrical scores:
        # Genuine:
        if self.enrol_s == self.probe_s and self.enrol_f == self.probe_f and self.enrol_i == self.probe_i:
            self.gen_identical = True
        if enrol_i < probe_i:
            self.gen_top = True
        if enrol_i > probe_i:
            self.gen_bott = True
        # Impostor:
        if self.enrol_s * 6 + self.enrol_f < self.probe_s * 6 + self.probe_f:
            self.imp_top = True
        if self.enrol_s * 6 + self.enrol_f > self.probe_s * 6 + self.probe_f:
            self.imp_bott = True
        # parameters for FVC protocols:
        if self.enrol_i == 1 and self.probe_i == 1:
            self.finger_1 = True
        if self.enrol_f == self.probe_f:
            self.same_finger = True

    def __str__(self):
        return "class Score object with parameters:\n" \
               "enrol_s          = {}\n" \
               "enrol_f          = {}\n" \
               "enrol_i          = {}\n" \
               "probe_s          = {}\n" \
               "probe_f          = {}\n" \
               "probe_i          = {}\n" \
               "score            = {}\n" \
               "genuine          = {}\n" \
               "imposter         = {}\n" \
               "test_class       = {}\n" \
               "dev_class        = {}\n" \
               "eval_class       = {}\n" \
               "parameter_tuning = {}\n" \
               "par_tun_any      = {}\n" \
               "gen_identical    = {}\n" \
               "gen_top          = {}\n" \
               "gen_bott         = {}\n" \
               "imp_top          = {}\n" \
               "imp_bott         = {}\n" \
               "finger_1         = {}\n" \
               "same_finger      = {}\n".format(self.enrol_s,
                                                self.enrol_f,
                                                self.enrol_i,
                                                self.probe_s,
                                                self.probe_f,
                                                self.probe_i,
                                                self.score,
                                                self.genuine,
                                                self.imposter,
                                                self.test_class,
                                                self.dev_class,
                                                self.eval_class,
                                                self.parameter_tuning,
                                                self.par_tun_any,
                                                self.gen_identical,
                                                self.gen_top,
                                                self.gen_bott,
                                                self.imp_top,
                                                self.imp_bott,
                                                self.finger_1,
                                                self.same_finger)


def fig_create():
    """
    Function ``fig_create()`` creats a matplotlib figure, with a predifined size, line colours and line styles, so that
    later we can plot ROC curves, maintaining a certain style.
    Function returns:
        f: (matplotlib.figure.Figure) object
        ax: (matplotlib.axes._subplots.AxesSubplot) object.
    these returned objects are later used to access the figure.
    """
    f, ax = plt.subplots(1, 1, figsize=(8, 5))
    from cycler import cycler
    ls_cycler = cycler('linestyle',
                       [(0, ()),  # solid
                        (0, (1, 5)),  # dotted
                        (0, (3, 1, 1, 1, 1, 1)),  # densely dash-dot-dotted
                        (0, (1, 1)),  # densely dotted
                        (0, (5, 1)),  # densely dashed
                        (0, (3, 5, 1, 5)),  # dash-dotted
                        (0, (3, 1, 1, 1))]  # densely dash-dotted
                       )
    k = len(ls_cycler)
    color_cycler = cycler('color', [plt.get_cmap('viridis')(i / k) for i in range(k)])
    new_cycler = color_cycler + ls_cycler
    ax.set_prop_cycle(new_cycler)
    return f, ax


def fig_save(f, ax, fname, title):
    """
    Function ``fig_save(f, ax, fname, title)`` saves a given figure. Intended to use together with ``fig_create()``
    function.
        Parameters:
            f: (matplotlib.figure.Figure) object handle (e.g., generated by ``fig_create()`` function);
            ax: (matplotlib.axes._subplots.AxesSubplot) object handle (e.g., generated by ``fig_create()`` function);
            fname: (string) label under which the figure will be saved (in folder ``OUTPUT_PLOT_PATH``);
            title: (string) figure's title.
        Output:
            Function saves the figure in ``OUTPUT_PLOT_PATH`` directory.
    """
    plt.figure(f.number)
    ax.set_xlabel('FMR', fontsize=18)
    ax.set_ylabel('FNMR', fontsize=18)
    ax.grid(True)
    ax.set_title("ROC, {}".format(title), fontsize=22)
    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)
    ax.legend(loc=1, fontsize=18)
    longpath = os.path.join(OUTPUT_PLOT_PATH, fname + ".pdf")
    plt.savefig(longpath, bbox_inches='tight', pad_inches=0)
    print("    figure saved as:    {}.pdf".format(longpath))
    plt.close(f.number)


def fig_plot(negatives, positives, eer, label, figure=None):
    """
    Function ``fig_plot(negatives, positives, eer, label, figure=None)`` plots ROC curve and EER using positive and
    negative scores to an active Matplotlib figure or a given one. Designed to be used together with ``fig_create()``
    and ``fig_save()`` functions.
        Parameters:
            negatives: (list of float) impostor biometric comparison scores;
            positives: (list of float) genuine biometric comparison scores;
            eer: (float) EER (Equal Error Rate), given in percentage;
            label: (string) label of the plot;
            figure: (matplotlib.figure.Figure) Optional -- figure, to which data will be plotted.
        Output:
            None (the function plots ROC and EER to a figure).
    """
    if figure is not None:
        plt.figure(figure.number)
    x = bob.measure.plot.roc(negatives=negatives,
                             positives=positives,
                             npoints=20000,
                             semilogx=True,
                             linewidth=3,
                             label=label)
    c = x[0].get_color()
    # previously we multiplied EER by 100, to work with percents, here we have to convert it back:
    plt.plot(eer / 100, eer / 100, 'o', markerfacecolor=c, markeredgecolor='black', markersize=14, alpha=0.6)


def display_result(gen_entries, imp_entries, label, figures, threshold=None, return_threshold=False):
    """
    Function display_result(gen_entries, imp_entries, label, figures, threshold=None, return_threshold=False). Function
    automate the EER calculation and ROC curve plotting for the UTFVP database various protocol experiments.
        Parameters:
            gen_entries      : (list of Score objects) genuine Score object list;
            imp_entries      : (list of Score objects) impostor Score object list;
            label            : (string) label used for the ROC plots;
            figures          : (matplotlib.figure.Figure or list of matplotlib.figure.Figure) figure handle(s) where the
                               ROC curve will be plotted. Muliple figure option designed, in order to plot a curve in
                               multiple plots;
            threshold        : (float) Optional parameter. If provided, this threshold is used to calculate FAR and FRR
                               values, which are averaged and returned as EER (HTER) value;
            return_threshold : (bool) Optional parameter. If set to ``True``, also threshold is being returned.
        Output:
            eer              : (float) Equal Error Rate (or HTER, if a threshold is provided);
            threshold        : (float) optional parameter, returned if ``return_threshold`` input parameter is set to
                               ``True``. Decision threshold, at which the FAR and FRR is equal (system's EER point).
    """
    gen_scores = [x.score for x in gen_entries]
    imp_scores = [x.score for x in imp_entries]
    gen_classes = len(list(set([x.enrol_s * 6 + x.enrol_f for x in gen_entries])))
    if threshold is None:
        eer = bob.measure.eer(negatives=imp_scores, positives=gen_scores, also_farfrr=False)
        eer = eer * 100
    else:
        far, frr = bob.measure.farfrr(negatives=imp_scores, positives=gen_scores, threshold=threshold)
        eer = (far + frr) * 100 / 2
    if isinstance(figures, mpl_fig.Figure) is True:
        fig_plot(negatives=imp_scores, positives=gen_scores, eer=eer, label=label, figure=figures)
    else:
        for f in figures:
            fig_plot(negatives=imp_scores, positives=gen_scores, eer=eer, label=label, figure=f)
    if USE_LATEX is True:  # longer protocol names:
        print("{:>33s}    {:>3d}    {:>4d}    {:>7d}    {:0.4f}".format(label,
                                                                        gen_classes,
                                                                        len(gen_scores),
                                                                        len(imp_scores),
                                                                        eer))
    else:  # shorter protocol names:
        print("{:>18s}    {:>3d}    {:>4d}    {:>7d}    {:0.4f}".format(label,
                                                                        gen_classes,
                                                                        len(gen_scores),
                                                                        len(imp_scores),
                                                                        eer))
    if return_threshold is True:
        threshold = bob.measure.eer_threshold(negatives=imp_scores, positives=gen_scores)
        return eer, threshold
    else:
        return eer
