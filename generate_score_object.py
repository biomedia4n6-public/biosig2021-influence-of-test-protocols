#!/usr/bin/env python3
"""
generate_score_object.py -- File use is optional, because the Pickle containing score descriptions is provided.
Script imports the score files for the given location ``DATABASE_SCORE_DIRECTORY`` (defined in utils.py) and save scores
as Python Pickle file.

Author -- Teodors Eglitis, teodors.eglitis@uniroma3.it
"""

from utils import *

print("File directory -- {}".format(DATABASE_SCORE_DIRECTORY))
all_files = os.listdir(DATABASE_SCORE_DIRECTORY)
txt_files = list(filter(lambda x: x[-4:] == '.txt', all_files))
if len(txt_files) != 1440:
    raise ValueError("In directory\n       {}\n should be 1440 files, but there are {} files.\n"
                     "Please fix this.".format(DATABASE_SCORE_DIRECTORY, len(txt_files)))
txt_files.sort()

SCORE_LIST = []
for i, f in enumerate(txt_files, start=1):
    if np.mod(i, 50) == 0:
        print("     reading file {} from {}".format(i, len(txt_files)))
    if f.startswith("0"):
        score_file = os.path.join(DATABASE_SCORE_DIRECTORY, f)
        enrol_s, enrol_f, enrol_i = list(map(int, f.split(".")[0].split("_")))
        with open(score_file, 'r') as reader:
            for line in reader.readlines():
                if line.endswith("\n"):
                    line = line[:-1]
                line = line.split(" ")
                score = np.float(line[3])
                probe_s, probe_f, probe_i = list(map(int, line[2].split("/")[1].split("_")[0:3]))
                SCORE_LIST.append(Score(enrol_s, enrol_f, enrol_i, probe_s, probe_f, probe_i, score))
print("All 1440 files successfully read!")
with open(SCORE_OBJECT_FILE_PATH, "wb") as f:
    cPickle.dump(SCORE_LIST, f)

print("Data saved to {}".format(SCORE_OBJECT_FILE_PATH))
print("All done. Now you can run ``generate_results.py`` file, to generate EER result table and ROC plots!")
print("Good luck!")
