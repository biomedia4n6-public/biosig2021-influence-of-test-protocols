#!/usr/bin/env python3
"""
generate_results.py -- the main script to reproduce results published in research paper ``Influence of Test Protocols on
Biometric Recognition Performance Estimation``.
Please read the README file first.

Author -- Teodors Eglitis, teodors.eglitis@uniroma3.it
"""
from utils import *

plt.close('all')

with open(SCORE_OBJECT_FILE_PATH, "rb") as f:
    print("Loading data file:\t{} (this will take up to 2 minutes)...".format(SCORE_OBJECT_FILE_PATH))
    SCORE_LIST = cPickle.load(f)
    print("Loading done!")

# creating figure objects -- we will need them to plot ROC curves.
f_summary, ax_summary = fig_create()
f_full, ax_full = fig_create()
f_original, ax_original = fig_create()
f_fvc, ax_fvc = fig_create()
f_fvc_short, ax_fvc_short = fig_create()
f_nom, ax_nom = fig_create()

experiment_definitions = {
    'latex_name': [],
    'simple_name': [],
    'gen_entries': [],
    'imp_entries': [],
    'figures': []}
# Experiment FULL:
experiment_definitions['latex_name'].append("full")
experiment_definitions['simple_name'].append("full")
experiment_definitions['figures'].append([f_full, f_summary])
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True])

# Experiment 1 VS ALL:
experiment_definitions['latex_name'].append("1vsall")
experiment_definitions['simple_name'].append("1vsall")
experiment_definitions['figures'].append(f_full)
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and x.par_tun_any is False])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and x.par_tun_any is False])

# Experiment PARAMETER TUNING:
experiment_definitions['latex_name'].append("parameter tuning")
experiment_definitions['simple_name'].append("parameter tuning")
experiment_definitions['figures'].append(f_full)
experiment_definitions['gen_entries'].append(
    [x for x in SCORE_LIST if x.genuine is True and x.parameter_tuning is True and x.gen_top is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and
                                              x.parameter_tuning is True and
                                              x.imp_top is True])

# Experiment original 1:
experiment_definitions['latex_name'].append("original$_{\\trigTw};$\\tb")
experiment_definitions['simple_name'].append("original 1")
experiment_definitions['figures'].append([f_original, f_summary])
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and
                                              x.par_tun_any is False and x.gen_top is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and x.par_tun_any is False])

# Experiment original 2:
experiment_definitions['latex_name'].append("original$_{\\trigTw};_{\\trigTb}$")
experiment_definitions['simple_name'].append("original 2")
experiment_definitions['figures'].append(f_original)
experiment_definitions['gen_entries'].append(
    [x for x in SCORE_LIST if x.genuine is True and x.par_tun_any is False and x.gen_top])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and
                                              x.par_tun_any is False and
                                              x.imp_top is True])

# Experiment original 3:
experiment_definitions['latex_name'].append("original$_{\\trigBw};_{\\trigBb}$")
experiment_definitions['simple_name'].append("original 3")
experiment_definitions['figures'].append(f_original)
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and
                                              x.par_tun_any is False and x.gen_bott is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and
                                              x.par_tun_any is False and x.imp_bott is True])

# Experiment FVC 1:
experiment_definitions['latex_name'].append("FVC$_{\\trigTw};$\\tb")
experiment_definitions['simple_name'].append("FVC 1")
experiment_definitions['figures'].append([f_fvc, f_summary])
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and x.gen_top is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and x.finger_1 is True])

# Experiment FVC 2:
experiment_definitions['latex_name'].append("FVC$_{\\trigTw};_{\\trigTb}$")
experiment_definitions['simple_name'].append("FVC 2")
experiment_definitions['figures'].append(f_fvc)
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and x.gen_top is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.imp_top is True])

# Experiment FVC 3:
experiment_definitions['latex_name'].append("FVC$_{\\trigBw};_{\\trigBb}$")
experiment_definitions['simple_name'].append("FVC 3")
experiment_definitions['figures'].append(f_fvc)
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and x.gen_bott is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.imp_bott is True])

# Experiment FVC 4:
experiment_definitions['latex_name'].append("FVC\\gentm$;$\\tb")
experiment_definitions['simple_name'].append("FVC 4")
experiment_definitions['figures'].append(f_fvc)
experiment_definitions['gen_entries'].append(
    [x for x in SCORE_LIST if x.genuine is True and (x.gen_top is True or x.gen_identical is True)])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and x.finger_1 is True])

# Experiment FVC 5:
experiment_definitions['latex_name'].append("FVC\\gentm$;_{\\trigTb}$")
experiment_definitions['simple_name'].append("FVC 5")
experiment_definitions['figures'].append(f_fvc)
experiment_definitions['gen_entries'].append(
    [x for x in SCORE_LIST if x.genuine is True and (x.gen_top is True or x.gen_identical is True)])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.imp_top is True])

# Experiment FVC 6:
experiment_definitions['latex_name'].append("FVC\\gentm$;_{\\trigBb}$")
experiment_definitions['simple_name'].append("FVC 6")
experiment_definitions['figures'].append(f_fvc)
experiment_definitions['gen_entries'].append(
    [x for x in SCORE_LIST if x.genuine is True and (x.gen_bott is True or x.gen_identical is True)])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.imp_bott is True])

# Experiment FVC short 1:
experiment_definitions['latex_name'].append("FVC_short$_{\\trigTw};$\\tb")
experiment_definitions['simple_name'].append("FVC short 1")
experiment_definitions['figures'].append([f_fvc_short, f_summary])
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and x.enrol_i < x.probe_i])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.enrol_f == x.probe_f])

# Experiment FVC short 2:
experiment_definitions['latex_name'].append("FVC_short$_{\\trigTw};_{\\trigTb}$")
experiment_definitions['simple_name'].append("FVC short 2")
experiment_definitions['figures'].append(f_fvc_short)
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and x.gen_top is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.imp_top is True and
                                              x.same_finger is True])

# Experiment FVC short 3:
experiment_definitions['latex_name'].append("FVC_short$_{\\trigBw};_{\\trigBb}$")
experiment_definitions['simple_name'].append("FVC short 3")
experiment_definitions['figures'].append(f_fvc_short)
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and x.gen_bott is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.imp_bott is True and
                                              x.same_finger is True])

# Experiment FVC short 4:
experiment_definitions['latex_name'].append("FVC_short\\gentm$;$\\tb")
experiment_definitions['simple_name'].append("FVC short 4")
experiment_definitions['figures'].append(f_fvc_short)
experiment_definitions['gen_entries'].append(
    [x for x in SCORE_LIST if x.genuine is True and (x.gen_top is True or x.gen_identical is True)])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.same_finger is True])

# Experiment FVC short 5:
experiment_definitions['latex_name'].append("FVC_short\\gentm$;_{\\trigTb}$")
experiment_definitions['simple_name'].append("FVC short 5")
experiment_definitions['figures'].append(f_fvc_short)
experiment_definitions['gen_entries'].append(
    [x for x in SCORE_LIST if x.genuine is True and (x.gen_top is True or x.gen_identical is True)])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.imp_top is True and
                                              x.same_finger is True])

# Experiment FVC short 6:
experiment_definitions['latex_name'].append("FVC_short\\gentm$;_{\\trigBb}$")
experiment_definitions['simple_name'].append("FVC short 6")
experiment_definitions['figures'].append(f_fvc_short)
experiment_definitions['gen_entries'].append(
    [x for x in SCORE_LIST if x.genuine is True and (x.gen_bott is True or x.gen_identical is True)])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if
                                              x.imposter is True and
                                              x.finger_1 is True and
                                              x.imp_bott is True and
                                              x.same_finger is True])

# Experiment nom 1:
experiment_definitions['latex_name'].append("nom$_{\\text{dev}}$")
experiment_definitions['simple_name'].append("nom 1")
experiment_definitions['figures'].append(f_nom)
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and x.dev_class is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and x.dev_class is True])

# Experiment nom 2:
experiment_definitions['latex_name'].append("nom$_{\\text{eval}}$")
experiment_definitions['simple_name'].append("nom 2")
experiment_definitions['figures'].append(f_nom)
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and x.eval_class is True])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and x.eval_class is True])

# Experiment nom 3:
experiment_definitions['latex_name'].append("nom$_{360_{12/34}}$")
experiment_definitions['simple_name'].append("nom 3")
experiment_definitions['figures'].append([f_nom, f_summary])
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and
                                              x.enrol_i in range(1, 3) and x.probe_i in range(3, 5)])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and
                                              x.enrol_i in range(1, 3) and x.probe_i in range(3, 5)])

# Experiment nom 4:
experiment_definitions['latex_name'].append("nom$_{360_{34/12}}$")
experiment_definitions['simple_name'].append("nom 4")
experiment_definitions['figures'].append(f_nom)
experiment_definitions['gen_entries'].append([x for x in SCORE_LIST if x.genuine is True and
                                              x.enrol_i in range(3, 5) and
                                              x.probe_i in range(1, 3)])
experiment_definitions['imp_entries'].append([x for x in SCORE_LIST if x.imposter is True and
                                              x.enrol_i in range(3, 5) and
                                              x.probe_i in range(1, 3)])

if USE_LATEX is True:
    experiment_definitions['label'] = experiment_definitions['latex_name']
else:
    experiment_definitions['label'] = experiment_definitions['simple_name']

EER_SCORE_TEST = []
if USE_LATEX is True:
    print("                        PROTOCOL  CLASSES  GEN COMP  IMP COMP    EER, %")
else:
    print("        PROTOCOL  CLASSES  GEN COMP  IMP COMP    EER, %")

for n, label in enumerate(experiment_definitions['label']):
    gen_entries = experiment_definitions['gen_entries'][n]
    imp_entries = experiment_definitions['imp_entries'][n]
    figures = experiment_definitions['figures'][n]
    # we need to get the threshold from "nom 1" experiment:
    if experiment_definitions['simple_name'][n] == "nom 1":
        eer, threshold = display_result(gen_entries=gen_entries,
                                        imp_entries=imp_entries,
                                        label=label,
                                        figures=figures,
                                        return_threshold=True)
    # We need to use the acquired threshold:
    elif experiment_definitions['simple_name'][n] == "nom 2":
        eer = display_result(gen_entries=gen_entries,
                             imp_entries=imp_entries,
                             label=label,
                             figures=figures,
                             threshold=threshold)
    else:
        eer = display_result(gen_entries=gen_entries,
                             imp_entries=imp_entries,
                             label=label,
                             figures=figures)
    EER_SCORE_TEST.append(eer)

print("Saving figures in {} directory:".format(OUTPUT_PLOT_PATH))

fig_save(f=f_summary, ax=ax_summary, fname="full", title="full group")
fig_save(f=f_full, ax=ax_full, fname="original", title="original group")
fig_save(f=f_original, ax=ax_original, fname="FVC", title="FVC group")
fig_save(f=f_fvc, ax=ax_fvc, fname="FVC_short", title="FVC short group")
fig_save(f=f_fvc_short, ax=ax_fvc_short, fname="nom", title="NOM group")
fig_save(f=f_nom, ax=ax_nom, fname="summary_plot", title="Different protocol ROC summary")

ORIGINAL_EER_SCORES = [0.6237910089755494, 0.6730769230769231,
                       0.11029411764705882, 0.9149453941120609,
                       0.9230769230769231, 0.7692307692307693,
                       0.8796941091509337, 0.8793072320231095,
                       0.6946378830083566, 0.5555555555555556,
                       0.5779557412565769, 0.4968276075518415,
                       0.9267106089139988, 0.9243565599497803,
                       0.7423101067168864, 0.5835687382297552,
                       0.5835687382297552, 0.5508474576271186,
                       0.49541363793700244, 1.0781386343804538,
                       0.903164654905602, 0.8333333333333334]

try:
    assert np.allclose(ORIGINAL_EER_SCORES, EER_SCORE_TEST, rtol=1e-05)
except AssertionError:
    print("The newly calculated scores doesn't match the values calculated originally.\n"
          "Somewhere should be an error.")
    print("Newly acquired score comparison with the originally acquired ones:")
    print("       Protocol            original    new        error")
    for n, i in enumerate(EER_SCORE_TEST):
        print("    {:<20s}   {:0.4f};   {:0.4f};   error = {}".format(experiment_definitions['simple_name'][n],
                                                                      ORIGINAL_EER_SCORES[n],
                                                                      i,
                                                                      np.abs(ORIGINAL_EER_SCORES[n] - i)))
else:
    print("Success! The newly calculated scores matches the originally calculated.\n\n\tBye now!")
